/* jslint esversion: 6 */
const themeDir = __dirname + '/../../';

const { purgeCSSPlugin } = require('@fullhuman/postcss-purgecss');

const purgecss = purgeCSSPlugin({
  // see https://gohugo.io/hugo-pipes/postprocess/#css-purging-with-postcss
  content: [
    '**/*/hugo_stats.json',
  ],
  defaultExtractor: (content) => {
    const els = JSON.parse(content).htmlElements;
    return [...(els.tags || []), ...(els.classes || []), ...(els.ids || [])];
  },
  safelist: [],
})

module.exports = {
  plugins: [
    require('postcss-import')({
      path: [themeDir]
    }),
    require('tailwindcss')(themeDir + 'assets/css/tailwind.config.js'),
    require('autoprefixer')({
      path: [themeDir]
    }),
    // FIXME purgecss is on the fritz.
    // ...(process.env.HUGO_ENVIRONMENT === 'production' ? [purgecss] : [])
  ]
}
