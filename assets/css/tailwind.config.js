/* jshint esversion: 9 */

// https://tailwindcss.com/docs/customizing-colors#color-palette-reference
const allColors = require('tailwindcss/colors');

const colors = {
  ...allColors,
  theme: {
    // Custom names go here.  Choose carefully.  It's likely easier to use the
    // Tailwind colors built-in and override them.
  }
};

const round = (num) =>
      num
      .toFixed(7)
      .replace(/(\.[0-9]+?)0+$/, '$1')
      .replace(/\.0$/, '');
const rem = (px) => `${round(px / 16)}rem`;
const em = (px, base) => `${round(px / base)}em`;

module.exports = {
  content: [
    './hugo_stats.json',
    './layouts/**/*.html',
  ],
  theme: {
    colors,
    extend: {
      typography: {
        DEFAULT: {
          css: {
            /* https://github.com/tailwindlabs/tailwindcss-typography/issues/18 */
            'code::before': {
              content: 'none',
            },
            'code::after': {
              content: 'none',
            },

            code: {
              fontWeight: '500',
              color: allColors.gray[800],
            },

            'a:hover code, a:focus code': {
              backgroundColor: 'inherit',
              color: 'inherit',
            },

            'ul > li::before': {
              backgroundColor: allColors.gray[400],
            },
            blockquote: {
              borderLeftColor: allColors.gray[300],
            },
            hr: {
              borderColor: allColors.gray[300],
            },

          }
        }
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ]
};
