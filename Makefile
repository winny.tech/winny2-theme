HUGO_ARGS=-s exampleSite --themesDir=../.. --logLevel info
HUGO=hugo

dev: prepare
	$(HUGO) server $(HUGO_ARGS) --disableFastRender --watch

ci:
	cat /etc/alpine-release
	cat /etc/apk/world
	apk add hugo git nodejs npm
	git config --global --add safe.directory $(PWD)
	$(MAKE) public

prepare: node_modules

public: prepare
	$(HUGO) $(HUGO_ARGS)

node_modules: package.json package-lock.json
	npm i

upgrade:
	npm upgrade
	$(MAKE) node_modules
	$(HUGO) $(HUGO_ARGS)

clean:
	rm -rf node_modules
	rm -rf exampleSite/public

.PHONY: dev prepare clean upgrade ci
